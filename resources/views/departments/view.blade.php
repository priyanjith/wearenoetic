@extends('adminlte::page')

@section('title', 'Department Manager')

@section('content_header')
    <h1>Department Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    <div class="form-group">
        <label for="name">Name</label>
        <p for="name">{{ isset($department) ? $department->name : "-" }}</p>
    </div>
    <a href="/admin/departments">
        <button type="button" class="btn btn-info">Back</button>
    </a>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop