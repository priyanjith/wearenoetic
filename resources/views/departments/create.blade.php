@extends('adminlte::page')

@section('title', 'Department Manager')

@section('content_header')
    <h1>Department Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    {{  Form::open(array('url'=>'admin/departments/store', 'method' => 'post')) }}
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control"  placeholder="Name">
            @if ( $errors->has('name') )
                <span class="text-danger">{{{ $errors->first('name') }}}</span>
            @endif
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    {{ Form::close() }}

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop