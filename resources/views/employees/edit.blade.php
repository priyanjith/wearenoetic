@extends('adminlte::page')

@section('title', 'Employee Manager')

@section('content_header')
    <h1>Employee Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    {{  Form::open(array('url'=>'admin/employees/update/'.$employee->id, 'method' => 'post', 'enctype' => 'multipart/form-data')) }}

    <div class="form-group">
        <label for="first_name">First Name</label>
        <input type="text" name="first_name" class="form-control" placeholder="First Name"
               value="{{ Input::old('first_name', isset($employee) ? $employee->first_name : null) }}">
        @if ( $errors->has('first_name') )
            <span class="text-danger">{{ $errors->first('first_name') }}</span>
        @endif
    </div>
    <div class="form-group">
        <label for="last_name">Last Name</label>
        <input type="text" name="last_name" class="form-control" placeholder="Last Name"
               value="{{ Input::old('last_name', isset($employee) ? $employee->last_name : null) }}">
        @if ( $errors->has('last_name') )
            <span class="text-danger">{{ $errors->first('last_name') }}</span>
        @endif
    </div>

    <div class="form-group">
        <label for="department_id">Department</label>
        {!! Form::select('department_id',$allDepartments, isset($employee) ? $employee->department_id : 0  , ['class'=>'form-control', 'placeholder'=>'Select Department']) !!}
        @if ( $errors->has('department_id') )
            <span class="text-danger">{{ $errors->first('department_id') }}</span>
        @endif
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    {{ Form::close() }}

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
@section('js')

@stop