@extends('adminlte::page')

@section('title', 'Employee Manager')

@section('content_header')
    <h1>Employee Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    {{  Form::open(array('url'=>'admin/employees/store', 'method' => 'post', 'enctype' => 'multipart/form-data')) }}
    <div class="form-group">
        <label for="first_name">First Name</label>
        <input type="text" name="first_name" class="form-control" placeholder="First Name">
        @if ( $errors->has('first_name') )
            <span class="text-danger">{{{ $errors->first('first_name') }}}</span>
        @endif
    </div>
    <div class="form-group">
        <label for="last_name">Last Name</label>
        <input type="text" name="last_name" class="form-control" placeholder="Last Name">
        @if ( $errors->has('last_name') )
            <span class="text-danger">{{{ $errors->last('first_name') }}}</span>
        @endif
    </div>

    <div class="form-group">
        <label for="title">Department</label>
        {!! Form::select('department_id',$allDepartments, old('department_id'), ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}
        @if ( $errors->has('department_id') )
            <span class="text-danger">{{{ $errors->first('department_id') }}}</span>
        @endif
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    {{ Form::close() }}

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop