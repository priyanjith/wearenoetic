@extends('adminlte::page')

@section('title', 'Employee Manager')

@section('content_header')
    <h1>Employee Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')

    <div class="form-group">
        <label for="first_name">First Name</label>
        <p for="title">{{ isset($employee) ? $employee->first_name : "-" }}</p>
    </div>
    <div class="form-group">
        <label for="last_name">Last Name</label>
        <p for="title">{{ isset($employee) ? $employee->last_name : "-" }}</p>
    </div>

    <div class="form-group">
        <label for="department">Department</label>
        <p for="title">{{ isset($employee->department->name) ? $employee->department->name : "-" }}</p>
    </div>

    <a href="/admin/employees">
        <button type="button" class="btn btn-info">Back</button>
    </a>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
@section('js')
@stop