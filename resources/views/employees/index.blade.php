@extends('adminlte::page')

@section('title', 'Employee Manager')

@section('content_header')
    <h1>Employee Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="pull-right">
                <a href="/admin/employees/create"><button type="button" class="btn btn-primary">Create</button></a>
            </div>
        </div>
        <div class="box-body">
            <table id="user-list" class="display responsive nowrap" width="100%">
                <thead>
                <tr>
                    <th class="col-md-3">First Name</th>
                    <th class="col-md-3">Last Name</th>
                    <th class="col-md-3">Department</th>
                    <th class="col-md-3">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employees as $employee)
                    <tr style="height:60px;">
                        <td data-th="First Name">{{ $employee->first_name }}</td>
                        <td data-th="Last Name">{{ $employee->last_name }}</td>
                        <td data-th="Department">{{ $employee->department->name }}</td>
                        <td data-th="Actions">
                            <div class="btn-group">
                                <a href="/admin/employees/show/{{ $employee->id }}">
                                    <button type="button" class="btn btn-primary">Show</button>
                                </a>
                                <a href="/admin/employees/edit/{{ $employee->id }}">
                                    <button type="button" class="btn btn-info">Update</button>
                                </a>
                                <a href="/admin/employees/delete/{{ $employee->id }}">
                                    <button type="button" class="btn btn-danger">Remove</button>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        $(document).ready(function() {

            $('#user-list').DataTable();
        } );
    </script>
@stop