@extends('layouts.app')

@section('title', 'Employee')


@section('content')
    <div class="row">
        <div class="col-lg-4">
            <a href="/admin/employees" class="btn btn-success">Employee</a>
            <a href="/admin/departments" class="btn btn-success">Department</a>
        </div>
    </div>
@stop

@section('css')
@stop

@section('js')

@stop