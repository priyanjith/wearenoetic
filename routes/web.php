<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'DashboardController@index')->name('dashboard');

//Route::group(['middleware' => ['auth']], function() {
    Route::group(['prefix' => 'admin'], function () {
        $this->get('employees', 'EmployeeController@index');
        $this->get('employees/create', 'EmployeeController@create');
        $this->post('employees/store', 'EmployeeController@store');
        $this->get('employees/edit/{id}', 'EmployeeController@edit');
        $this->post('employees/update/{id}', 'EmployeeController@update');
        $this->get('employees/show/{id}', 'EmployeeController@show');
        $this->get('employees/delete/{id}', 'EmployeeController@destroy');
        $this->get('departments', 'DepartmentController@index');
        $this->get('departments/create', 'DepartmentController@create');
        $this->post('departments/store', 'DepartmentController@store');
        $this->get('departments/edit/{id}', 'DepartmentController@edit');
        $this->post('departments/update/{id}', 'DepartmentController@update');
        $this->get('departments/show/{id}', 'DepartmentController@show');
        $this->get('departments/delete/{id}', 'DepartmentController@destroy');
    });
//});