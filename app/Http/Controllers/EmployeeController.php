<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the employees
        $employees = Employee::all();

        // load the view and pass the employees
        return \View::make('employees.index')
            ->with('employees', $employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $allDepartments = Department::pluck('name', 'id')->all();
        $data = [
            'allDepartments' => $allDepartments
        ];
        return view('employees.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'department_id' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('admin/employees/create')
                ->withErrors($validator);
        } else {

            try {
                // store
                $employee = new Employee;
                $employee->first_name = Input::get('first_name');
                $employee->last_name = Input::get('last_name');
                $employee->department_id = Input::get('department_id');
                $employee->save();

                // redirect
                Session::flash('message', 'Successfully created product!');
                return Redirect::to('admin/employees');
            } catch (\Exception $e) {
                return Redirect::to('admin/employees/create')
                    ->withErrors($validator);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);
        $data = [
            'employee' => $employee,
        ];
        return view('employees.view')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $allDepartments = Department::pluck('name', 'id')->all();
        $employee = Employee::find($id);
        $data = [
            'allDepartments'      => $allDepartments,
            'employee'            => $employee,
        ];
        return view('employees.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $employee = Employee::find($id);
        // validate
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'department_id' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('admin/employees/edit/' . $id)
                ->withErrors($validator);
        } else {

            try {
                // store
                $employee->first_name = Input::get('first_name');
                $employee->last_name = Input::get('last_name');
                $employee->department_id = Input::get('department_id');
                $employee->save();

                // redirect
                Session::flash('message', 'Successfully edit product!');
                return Redirect::to('admin/employees');
            } catch (\Exception $e) {
                Session::flash('error', 'Error edit product!');
                return Redirect::to('admin/employees/edit/' . $id)
                    ->withErrors($validator);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $employee = Employee::find($id);
        $employee->delete();

        //To Do
        //Remove image folder

        // redirect
        Session::flash('message', 'Successfully deleted the product!');
        return Redirect::to('admin/employees');
    }
}
