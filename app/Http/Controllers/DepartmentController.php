<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Session;
use App\Department;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the departments
        $departments = Department::all();
        // load the view and pass the departments
        return \View::make('departments.index')
            ->with('departments', $departments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        $rules = array(
            'name' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('admin/departments/create')
                ->withErrors($validator);
        } else {
            // store
            $departments = new Department;
            $departments->name  = Input::get('name');
            $departments->save();

            // redirect
            Session::flash('message', 'Successfully created departments!');
            return Redirect::to('admin/departments');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $department = Department::find($id);
        // Show the page
        return view('departments.view', compact(
            'department'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $department = Department::find($id);

        // Show the page
        return view('departments.edit', compact(
            'department'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $departments = Department::find($id);
        // validate
        $rules = array(
            'name' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('admin/departments/edit/'.$id)
                ->withErrors($validator);
        } else {
            // store
            $departments->name  = Input::get('name');
            $departments->save();

            // redirect
            Session::flash('message', 'Successfully created departments!');
            return Redirect::to('admin/departments');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $departments = Department::find($id);
        $departments->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the departments!');
        return Redirect::to('admin/departments');
    }
}
